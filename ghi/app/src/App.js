import logo from './logo.svg';
import './App.css';
import React from "react";
import Nav from './Nav';

import LocationForm from './LocationForm';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import ConferenceForm from './ConferenceForm';
import AttendeesList from "./AttendeesList"
import PresentationForm from "./PresentationForm"
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
  <>
  <BrowserRouter>
    <Nav />
    <Routes>
      <Route index element={<MainPage />} />
      <Route path="/conferences/new" element={<ConferenceForm />} />
      <Route path="/locations/new" element={<LocationForm />} />
      <Route path="/presentations/new" element={<PresentationForm />} />
      <Route path="/attendees">
        <Route
          index
          element={<AttendeesList attendees={props.attendees}/>}
          />
      </Route>
      <Route path="/conferences/new" element={<ConferenceForm />} />
      <Route path="/attendees/new" element={<AttendConferenceForm />} />
    </Routes>
</BrowserRouter>
  </>
  );
}

export default App;
