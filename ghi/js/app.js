function createCard(name, description, pictureUrl, start_date, end_date, location) {
    const startDate = new Date(start_date);
    const endDate = new Date(end_date);
    return `
    <div class ="col-4">
      <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          <small class="text-body secondary">${String(startDate.getMonth()+1)}/${String(startDate.getDay()+24)}/${String(startDate.getFullYear())}
          - ${String(endDate.getMonth()+1)}/${String(endDate.getDay()+24)}/${String(endDate.getFullYear())}</small>
          </div>
        </div>
      </div>
    </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const location = details.conference.location.name;
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const start_date = details.conference.starts;
              const end_date = details.conference.starts;
              const html = createCard(title, description, pictureUrl, start_date, end_date, location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }

        }
      } catch (e) {
        const html = errorMessage();
        const column = document.querySelector(".row");
        column.innerHTML = html;
        console.log("There is a problem in your request!");
      }
});
